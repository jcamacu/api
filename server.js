//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser')
app.use(bodyparser.json())

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./movimientosv2.json');

app.get('/',function (req, res) {
  //res.send("Hola Mundo nodeJS")
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Clientes/',function (req, res) {
  res.send("Aqui estan los clientes devueltos nuevos")
  //res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Clientes/:idcliente',function (req, res) {
  res.send("Aqui tiene al cliente numero: " + req.params.idcliente)
  //res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Movimientos/v1',function (req, res) {
  res.sendfile('movimientosv1.json')
  //res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Movimientos3/v2',function (req, res) {
  res.sendfile('movimientosv2.json')
  //res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Movimientos/v2/:index/:otroparametro',function (req, res) {
  console.log(req.params.index);
  console.log(req.params.otroparametro);
  res.send(movimientosJSON[req.params.index]);
  //res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Movimientosq/v2/',function (req, res) {
  console.log(req.query);

  res.send('Recibido');
  //res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Movimientos/v2/',function (req, res) {
  res.send(movimientosJSON);
  //res.sendFile(path.join(__dirname,'index.html'));
});


app.post('/',function (req, res) {
  res.send("Hemos recibido su peticion post")
})

app.post('/Movimientos/v2',function (req, res) {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta!!!")
})
